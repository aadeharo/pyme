package com.empresa.pyme.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "Bancos")
public class Banco {
	
	@Id							//Asigna id = id de tabla
	@GeneratedValue				//Auto incremento del id
	private Long id;
	private String nombre;
	private String direccion;
	private String localidad;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "banco")
	private List<Empresa> empresas;
		
	public List<Empresa> getEmpresa() {
		return empresas;
	}
	public void setEmpresa(List<Empresa> empresa) {
		this.empresas = empresa;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getLocalidad() {
		return localidad;
	}
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	
	
	

}
