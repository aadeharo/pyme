package com.empresa.pyme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PymeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PymeApplication.class, args);
	}

}
