package com.empresa.pyme.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empresa.pyme.model.Banco;
import com.empresa.pyme.model.Empleado;
import com.empresa.pyme.model.Empresa;
import com.empresa.pyme.repository.DaoBanco;
import com.empresa.pyme.repository.DaoEmpleado;
import com.empresa.pyme.repository.DaoEmpresa;

@RestController
@RequestMapping({"/pyme"})
public class MainController {
	
	@Autowired			//Automatiza la instanciacion
	private DaoEmpresa daoempresa;
	@Autowired
	private DaoBanco daobanco;
	@Autowired
	private DaoEmpleado daoempleado;
	
	
	//	Crear banco
	@PostMapping (path = {"/banco"})
	public Banco create(@RequestBody Banco banco)
	{	
		return daobanco.save(banco);	
	}
	
	//	Crear empresa
	//  Asigna un id_banco al momento de crear la empresa
	@PostMapping (path = {"/empresa/{id}"})
	public ResponseEntity<Object> createEmpresa(@RequestBody Empresa empresa, @PathVariable("id") long id_banco)
	{	
		Banco bank = daobanco.findById(id_banco).orElse(null);		//Busca banco segun id ingresada
		Empresa emp = new Empresa();								//Crea Empresa nueva
		
		if( bank != null )
		{
			emp.setNombre(empresa.getNombre());						//Setea nombre
			emp.setDireccion(empresa.getDireccion());				//Setea direccion
			emp.setLocalidad(empresa.getLocalidad());				//Localidad
			emp.setDesc(empresa.getDesc());							//Descripcion de la empresa
		
			Empresa pyme = daoempresa.save(emp);
						
			JSONObject obj = new JSONObject();
			obj.put("error", 0);
			obj.put("Empresa",pyme.getNombre());						//Datos de empresa y mensaje de error = 0
			
			return ResponseEntity.ok().body(obj.toString());
			
		} else {
			
			JSONObject obj = new JSONObject();
			obj.put("error", 1);
			obj.put("Message","No se encontro el banco");
			
			return ResponseEntity.ok().body(obj.toString());	
		}
	}
	
	@PostMapping (path = {"/empleado/{id}"})
	public ResponseEntity<Object> createEmpleado(@RequestBody Empleado empleado, @PathVariable("id") long id)
	{	
		Empresa empresa = daoempresa.findById(id).orElse(null);
		Empleado emple = new Empleado();
			
		if( empresa != null )
		{
			emple.setNombre(empleado.getNombre());
			emple.setApellido(empleado.getApellido());
			emple.setDni(empleado.getDni());
			emple.setDireccion(empleado.getDireccion());
			emple.setEmpresa(empleado.getEmpresa());
			
			Empleado E1 = daoempleado.save(emple);
						
			JSONObject obj = new JSONObject();
			obj.put("error", 0);
			obj.put("Empresa",E1.getNombre());
			
			return ResponseEntity.ok().body(obj.toString());
			
		} else {
			
			JSONObject obj = new JSONObject();
			obj.put("error", 1);
			obj.put("Message","No se encontro la empresa");
			
			return ResponseEntity.ok().body(obj.toString());	
		}
	}
	
	
	@GetMapping (path = {"/lista/{id}"})
	public ResponseEntity<Object> getListaEmpleados(@PathVariable Long id)
	{
		Empresa emp = daoempresa.findById(id).orElse(null);
		List<Empleado> empleadosFound = daoempleado.findAll();
		
		JSONArray json_array = new JSONArray();
		
		if( empleadosFound.size() > 0)
		{
			for(Empleado emple: empleadosFound)
			{
				JSONObject aux = new JSONObject();
				aux.put("Nombre",emple.getNombre());
				aux.put("Apellido",emple.getApellido());
				json_array.put(aux);
			}
			
			JSONObject obj = new JSONObject();
			obj.put("error", 0);
			obj.put("Lista de empleados de la empresa "+emp.getNombre(), json_array);
			
			return ResponseEntity.ok().body(obj.toString());
		
		} else {
			JSONObject obj = new JSONObject();
			obj.put("error", 1);
			obj.put("Message","No se encontro el comentario o el post");
			
			return ResponseEntity.ok().body(obj.toString());
		}
		
		
	}
	
	@PostMapping (path = {"/direccion/{id}/{new_adress}"})
	public ResponseEntity<Object> cambiarDireccion(@PathVariable Long id, @PathVariable("new_adress") String nueva)
	{
		Empleado emple = daoempleado.findById(id).orElse(null);
		
		emple.setDireccion(nueva);
		
		JSONObject obj = new JSONObject();
		obj.put("error", 0);
		obj.put("Message","Se cambio la direccion del usuario");
		
		return ResponseEntity.ok().body(obj.toString());
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
