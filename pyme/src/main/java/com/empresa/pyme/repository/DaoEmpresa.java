package com.empresa.pyme.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.empresa.pyme.model.Empresa;

public interface DaoEmpresa extends JpaRepository<Empresa, Long> {
	
}
