package com.empresa.pyme.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.empresa.pyme.model.Banco;

public interface DaoBanco extends JpaRepository<Banco, Long> {

	
}
