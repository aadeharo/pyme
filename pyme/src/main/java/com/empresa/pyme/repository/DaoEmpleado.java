package com.empresa.pyme.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.empresa.pyme.model.Empleado;
import com.empresa.pyme.model.Empresa;

public interface DaoEmpleado extends JpaRepository<Empleado, Long> {

	public List<Empleado> findByEmpresa (Empresa emp);
	public List<Empleado> findByDni (String dni);
	
}
